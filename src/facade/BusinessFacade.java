/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import business.BMascota;
import business.BUser;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ronal
 */
public class BusinessFacade {
    
    private BUser bUser;
    private BMascota bMascota;

    public BusinessFacade() {      
        bUser = new BUser();
        bMascota = new BMascota();
    }
    
    //Bussines User
    public void saveUser(List<String> params) throws NumberFormatException, 
            SQLException, IndexOutOfBoundsException, ParseException{
        bUser.save(params);
    }
    
    public void modifyUser(List<String> params)throws NumberFormatException, 
            SQLException, IndexOutOfBoundsException, ParseException{
        bUser.modify(params);
    }
    
    public void deleteUser(List<String> params)throws NumberFormatException, 
            SQLException, IndexOutOfBoundsException, ParseException{
        bUser.delete(params);
    }
    
    public ArrayList<String[]> getUsers() throws NumberFormatException, 
            SQLException, IndexOutOfBoundsException, ParseException{
        return bUser.getList();
    }
    
    //Business Mascota
    public void saveMascota(List<String> params) throws SQLException,
            NumberFormatException, IndexOutOfBoundsException, ParseException {
        bMascota.save(params);
    }
    
    public void modifyMascota(List<String> params) throws SQLException,
            NumberFormatException, NumberFormatException, IndexOutOfBoundsException, 
            ParseException {
        bMascota.modify(params);
    }
    
}
