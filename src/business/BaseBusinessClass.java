/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ronal
 */
public abstract class BaseBusinessClass {

    public abstract void save(List<String> params) throws SQLException, NumberFormatException, IndexOutOfBoundsException, ParseException;
    
    public abstract void modify(List<String> params) throws SQLException, NumberFormatException, IndexOutOfBoundsException, ParseException;
    
    public abstract void delete(List<String> params) throws SQLException, NumberFormatException, IndexOutOfBoundsException, ParseException;
    
    public abstract ArrayList<String[]> getList() throws SQLException, NumberFormatException, IndexOutOfBoundsException, ParseException;
}
