/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import data.DUser;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import models.User;

/**
 *
 * @author ronal
 */
public class BUser extends BaseBusinessClass{
    
    private DUser dUser;

    public BUser() {
        dUser = new DUser();
    }

    @Override
    public void save(List<String> params) throws SQLException, 
            NumberFormatException, IndexOutOfBoundsException, ParseException {
        dUser.save(params.get(0), params.get(1), params.get(2), params.get(3), params.get(4), params.get(5), params.get(6));
        dUser.disconnect();
    }

    @Override
    public void modify(List<String> params) throws SQLException, NumberFormatException, IndexOutOfBoundsException, ParseException {
        dUser.modify(Integer.parseInt(params.get(0)), params.get(1), params.get(2), params.get(3), params.get(4), params.get(5), params.get(6), params.get(7));
        dUser.disconnect();
    }

    @Override
    public void delete(List<String> params) throws SQLException, NumberFormatException, IndexOutOfBoundsException, ParseException {
        dUser.delete(Integer.parseInt(params.get(0)));
        dUser.disconnect();
    }

    @Override
    public ArrayList<String[]> getList() throws SQLException, NumberFormatException, IndexOutOfBoundsException, ParseException {
        ArrayList<String[]> aux = new ArrayList<>();
        for(User user: dUser.get()){
            aux.add(new String[]{
                String.valueOf(user.getId()),
                user.getCi(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getGender().equals("MALE") ? "Masculino" : "Femenino",
                user.getBirthday(),
                user.getPhone()
            });
        }
        dUser.disconnect();
        return aux;
    }

    public boolean existsUser(String email) throws SQLException {
        boolean b = dUser.isExists(email);
        dUser.disconnect();
        return b;
    }

}
