/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import business.BMascota;
import business.BUser;
import facade.BusinessFacade;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ronal
 */
public class BusinessData {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {

            BusinessFacade bf = new BusinessFacade();
            List<String> user = new ArrayList<>();
            user.add("12788604");
            user.add("Ronaldo");
            user.add("Rivero Gonzales");
            user.add("ronaldorivero3@gmail.com");
            user.add("MALE");
            user.add("1996-06-23");
            user.add("76042142");
            bf.saveUser(user);
            
        } catch (NumberFormatException ex) {
            Logger.getLogger(BusinessData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(BusinessData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IndexOutOfBoundsException ex) {
            Logger.getLogger(BusinessData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(BusinessData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
