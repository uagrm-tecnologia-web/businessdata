/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import models.User;
import models.utils.DateString;
import sqlconnectionlibrary.SQLConnection;

/**
 *
 * @author ronal
 */
public class DUser {
    private SQLConnection connection;
    
    public DUser(){
        connection = new SQLConnection();
    }
    
    public void save(String ci, String first_name, String last_name, 
            String email, String gender, String birthday, String phone)throws SQLException, ParseException{
        String query = "INSERT INTO users(ci, first_name, last_name, "
            + "email, gender, birthday, phone) VALUES(?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = connection.connect().prepareStatement(query);
        ps.setString(1, ci);
        ps.setString(2, first_name);
        ps.setString(3, last_name);
        ps.setString(4, email);
        ps.setString(5, gender);
        ps.setDate(6, DateString.StringToDateSQL(birthday));
        ps.setString(7, phone);

        if(ps.executeUpdate() == 0){
            System.err.println("Class DUser.save dice: \n"
                    + "Ocurrio un error al insertar el nuevo usuario.");
            throw new SQLException();
        }
    }
    
    public void modify(int id, String ci, String first_name, String last_name, 
            String email, String gender, String birthday, String phone)throws SQLException, ParseException{
        
        String query = "UPDATE users SET ci=?, first_name=?, last_name=?,"
            + " email=?, gender=?, birthday=?, phone=? WHERE id = ?";
        PreparedStatement ps = connection.connect().prepareStatement(query);
        ps.setString(1, ci);
        ps.setString(2, first_name);
        ps.setString(3, last_name);
        ps.setString(4, email);
        ps.setString(5, gender);
        ps.setDate(6, DateString.StringToDateSQL(birthday));
        ps.setString(7, phone);
        ps.setInt(8, id);

        if(ps.executeUpdate() == 0){
            System.err.println("Class DUser.modify dice: \n"
                    + "Ocurrio un error al modificar el usuario.");
            throw new SQLException();
        }
    }
    
    public void delete(int id)throws SQLException{
        String query = "DELETE FROM users WHERE id = ?";
        PreparedStatement ps = connection.connect().prepareStatement(query);
        ps.setInt(1, id);

        if(ps.executeUpdate() == 0){
            System.err.println("Class DUser.delete dice: \n"
                    + "Ocurrio un error al eliminar el usuario.");
            throw new SQLException();
        }
    }
    
    public List<User> get()throws SQLException{        
        List<User> users = new ArrayList<>();
        String query = "SELECT * FROM users";
        PreparedStatement ps = connection.connect().prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            users.add(new User(
                rs.getInt("id"), rs.getString("ci"), rs.getString("first_name"),
                rs.getString("last_name"), rs.getString("email"),
                rs.getString("gender"), rs.getString("birthday"), rs.getString("phone")
            ));
        }
        return users;
    }
    
    public boolean isExists(String email) throws SQLException{
        String query = "SELECT * FROM users WHERE email=?";
        PreparedStatement ps = connection.connect().prepareStatement(query);
        ps.setString(1, email);
        ResultSet rs = ps.executeQuery();
        return rs.next();
    }
    
    public void disconnect(){
        if(connection != null){
            connection.closeConnection();
        }
    }
}
